package com.gdstruct.quiz;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Player asuna = new Player(1,"Asuna",100);
        Player lorenzo = new Player(2,"lorenzo",205);
        Player anton = new Player(3,"anton",35);

        PlayerLinkedList playerLinkedList=new PlayerLinkedList();


        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lorenzo);
        playerLinkedList.addToFront(anton);


        playerLinkedList.remove();
        playerLinkedList.printList();

    }
}
