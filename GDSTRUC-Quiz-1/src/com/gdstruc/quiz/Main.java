package com.gdstruc.quiz;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int[] numbers = new int[10];

        numbers[0] = 25;
        numbers[1] = 28;
        numbers[2] = 20;
        numbers[3] = 24;
        numbers[4] = 22;
        numbers[5] = 21;
        numbers[6] = 99;
        numbers[7] = 90;
        numbers[8] = 6666;
        numbers[9] = 787;

        System.out.println("before bubble sort:");
        printArrayElements(numbers);

        bubbleSort(numbers);
        System.out.println("\n\n after bubble sort:");

        printArrayElements(numbers);
    }

    private static void bubbleSort(int[] arr) {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--) {
            for (int i = 0; i < lastSortedIndex; i++) {
                if (arr[i] < arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length-1;lastSortedIndex>0;lastSortedIndex--)
        {
            int largestIndex=0;

            for(int i=1;i<=lastSortedIndex;i++)
            {
                if (arr[i]<arr[largestIndex])
                {
                    largestIndex=i;
                }
            }

            int temp=arr[lastSortedIndex];
            arr[lastSortedIndex]=arr[largestIndex];
            arr[largestIndex]=temp;

        }
    }
    private static void printArrayElements(int[] arr) {
        for (int j : arr) {
            System.out.print(j + "  ");
        }
    }
}
