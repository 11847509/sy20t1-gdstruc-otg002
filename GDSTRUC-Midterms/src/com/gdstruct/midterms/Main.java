package com.gdstruct.midterms;

import java.util.EmptyStackException;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {


        CardStack stack = new CardStack(30);
        CardStack NewStack = new CardStack(30);
        CardStack DiscardedStack = new CardStack(30);

        //Numbers to be drawn using input
        // System.out.println("Input number of Cards to be drawn");
        //Scanner scanner = new Scanner(System.in);
        //int input = scanner.nextInt();

        Random rand = new Random();
        int rand_int1 = rand.nextInt(5) + 1;
        Random rand2 = new Random();
        int rand_int2 = rand.nextInt(5) + 1;
        Random rand3 = new Random();
        int rand_int3 = rand.nextInt(5) + 1;
        Random rand4 = new Random();
        int rand_int4 = rand.nextInt(3) + 1;

        stack.push(new Card(1, "A"));
        stack.push(new Card(2, "B"));
        stack.push(new Card(3, "C"));
        stack.push(new Card(4, "D"));
        stack.push(new Card(5, "E"));
        stack.push(new Card(6, "F"));
        stack.push(new Card(7, "G"));
        stack.push(new Card(8, "H"));
        stack.push(new Card(9, "I"));
        stack.push(new Card(10, "J"));
        stack.push(new Card(11, "K"));
        stack.push(new Card(12, "L"));
        stack.push(new Card(13, "M"));
        stack.push(new Card(14, "N"));
        stack.push(new Card(15, "O"));
        stack.push(new Card(16, "P"));
        stack.push(new Card(17, "Q"));
        stack.push(new Card(18, "R"));
        stack.push(new Card(19, "S"));
        stack.push(new Card(20, "T"));
        stack.push(new Card(21, "U"));
        stack.push(new Card(22, "V"));
        stack.push(new Card(23, "W"));
        stack.push(new Card(24, "X"));
        stack.push(new Card(25, "Y"));
        stack.push(new Card(26, "Z"));
        stack.push(new Card(27, "AA"));
        stack.push(new Card(28, "AB"));
        stack.push(new Card(29, "AC"));
        stack.push(new Card(30, "AD"));


       while (stack.getTop() > 0)
        {
           if(rand_int4==1)
           {
            for (int i = rand_int1; i > 0; i--)// Deck to hands//for
                // (int i = input; i > 0; i--)   // stack pop if input is used
            {
                System.out.println("Cards deducted from the deck: " + rand_int1);
                System.out.println("Getting cards from deck:");
                NewStack.push(stack.pop());
            }
            }
            if (rand_int4==2) {
                for (int i = rand_int2; i > 0; i--)//Hands to Discarded pile
                {
                    System.out.println("Cards deducted from the deck: " + rand_int2);
                    System.out.println("Discarding cards from hands:");
                    DiscardedStack.push(NewStack.pop());
                }
            }
            if(rand_int4==3)
            {
                for (int i = rand_int3; i > 0; i--)//Deck to Discarded
                {
                    {
                        System.out.println("Cards deducted from the deck: " + rand_int3);
                        System.out.println("Discarding cards from Deck");
                        DiscardedStack.push(stack.pop());
                    }
                }
            }

            //stack.push(stack.pop());x
            //System.out.println("Cards deducted from the deck: " + rand_int1);

            stack.printStack();
            System.out.println("Cards in hand Cards:");
            NewStack.printStack();

            System.out.println("Discarded Cards:");
            DiscardedStack.printStack();
        }
  }
}
