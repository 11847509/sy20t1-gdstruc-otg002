package com.gdstruct.midterms;

import java.util.EmptyStackException;

import java.util.Random;

public class CardStack {
    private Card[] stack;
    private Card[] NewStack;
    private int top;

    public int getTop() {
        return top;
    }

    Random rn = new Random();
    int answer = rn.nextInt(5) + 1;

    public CardStack(int capacity)
    {
        stack = new Card[capacity];
    }

    public void push(Card card )
    {
        if(top == stack.length)
        {
            Card[] NewStack= new Card[2* stack.length];
            System.arraycopy(stack,0, NewStack,0,stack.length);
            stack=NewStack;
        }
        stack[top++]=card;
    }

    public Card pop()
    {

        if(isEmpty())
        {
            throw  new EmptyStackException();
        }

        Card poppedCard =stack[--top];
        stack[top]=null;
        return poppedCard;

    }

    public Card Peek()
    {
        if(isEmpty())
        {
            throw new EmptyStackException();
        }
        return stack[top -1 ];
    }
    public boolean isEmpty()
    {
        return top==0;
    }


    public void printStack()
    {
        System.out.println("");
        for(int i=top -1;i>=0;i--)
        {
            System.out.println(stack[i]);
        }
    }
}
