package com.gdstruct.module5;

import java.util.Objects;

public class Player {

    private int playerId;
    private String userName;
    private int level;


    public Player(int id, String userName, int level) {
        this.playerId = id;
        this.userName = userName;
        this.level = level;
    }

    public int getId() {
        return playerId;
    }

    public void setId(int id) {
        this.playerId = id;
    }

    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + playerId +
                ", userName='" + userName + '\'' +
                ", level=" + level +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return playerId == player.playerId &&
                level == player.level &&
                Objects.equals(userName, player.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, userName, level);
    }
}
