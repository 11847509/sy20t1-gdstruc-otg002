package com.gdstruct.module5;

public class SimpleHashtable {
    private StoredPlayer[]hashtable;

    public SimpleHashtable()
    {
        hashtable=new StoredPlayer[10];
    }

    private int hashKey(String key)
    //to make sure that we are mapping the key within 0-10
    {
        return key.length() % hashtable.length;
    }

    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);

        if(isOccupied(hashedKey));
        {
            // do linear probing
            int stoppingIndex=hashedKey;

            if (hashedKey== hashtable.length-1)
            {
                hashedKey=0;
            }
            else
            {
                hashedKey++;
            }
            while(isOccupied(hashedKey)&& hashedKey!=stoppingIndex)
            {
                hashedKey= (hashedKey+1)% hashtable.length;
            }
        }

        if(isOccupied(hashedKey))
        {
            System.out.println("Sorry, there is already an element at position" + hashedKey);
        }
        else
        {
            hashtable[hashedKey]=new StoredPlayer(key,value);

        }
    }

    public Player get(String key)
    {
        int hashedKey=findKey(key);
        if(hashedKey== -1)
        {
            return null ;
        }
        return hashtable[hashedKey].value;
    }


    private int findKey(String key)
    {
        int hashedKey=hashKey(key);
        // if found the right key
        if(hashtable[hashedKey]!= null && hashtable[hashedKey].key.equals(key))
        {
        return  hashedKey;
        }


        // do linear probing
        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null && !hashtable[hashedKey].key.equals(key))

        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }
if(hashtable[hashedKey]!= null
&& hashtable[hashedKey].key.equals(key))
{
    return hashedKey;
}
return -1;
    }


    public Player remove(String key)
    {
        int hasheDkey;
        int hashedKey=findKey2(key);
        if(hashedKey== -1)
        {
          hasheDkey=hashedKey ;
        }
        return hashtable[hashedKey].value;
    }

    private int findKey2(String key)
    {
        int hashedKey=hashKey(key);
        // if found the right key
        if(hashtable[hashedKey]!= null && hashtable[hashedKey].key.equals(key))
        {
            return  hashedKey;
        }


        // do linear probing
        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null && !hashtable[hashedKey].key.equals(key))

        {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }
        if(hashtable[hashedKey]!= null
                && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }
        return -1;
    }
    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }


    public void printHashtable()
    {
        for( int i=0; i<hashtable.length; i++)
        {
            System.out.println("Element"+ i +" "+hashtable[i]);
        }
    }
}
